<?php
$importer = new Importer();
$importer->run();

class Importer {

  private $db;
  private $filesDir;
  private $files;

  public function __construct() {
    require_once __DIR__.'/../config.php';
    require_once __DIR__ . '/../lib/phpexcel/Classes/PHPExcel.php';
    $this->filesDir = __DIR__ . '/files';
    $this->files = array();
  }

  // Executa
  public function run() {
    print ("__SEMAS IMPORTER__\n");
    $this->checkDatabase();
    $this->checkFiles();
    $this->process();
    print ("__FIM__\n");
  }

  private function checkDatabase() {
    print "Verificando base de dados...\n";
    try {
      $this->db = new PDO("mysql:host=".DB_HOST, DB_USER, DB_PASS, array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
      ));
      if ($this->db) {
        print "Base de dados conectada com sucesso !\n";
      } else {
        print "Nao foi possivel conectar na base de dados. Verifique os parametros de conexao e tente novamente.\n";
        print "Mensagem de erro: " . implode(',', $this->db->errorInfo()) . "\n";
        exit;
      }
    } catch (Exception $ex) {
      print "Erro ao tentar conectar a base de dados. Verifique os parametros de conexao e tente novamente.\n";
      print "Mensagem de erro: " . $ex->getMessage() . "\n";
      exit;
    }
  }

  private function checkFiles() {
    echo "Verificando arquivos disponiveis...\n";
    $dh = opendir($this->filesDir);

    while (false !== ($filename = readdir($dh))) {
      if (substr($filename, -4) == '.xls' || substr($filename, -5) == '.xlsx') {
        $this->files[] = $filename;
      }
    }
    closedir($dh);

    $count = count($this->files);
    if ($count == 0) {
      exit("Não há nenhuma planilha na pasta dos arquivos. Verifique a pasta e tente novamente.\n");
    }

    echo "Existem {$count} planilhas a serem processadas na pasta de arquivos. Continuar o procedimento ?\n";
    echo "Digite qualquer tecla pra continuar ou 'n' para abortar: ";
    $ch = fopen("php://stdin", "r");
    $ansr = fgets($ch);
    if (trim($ansr) == 'n') {
      echo "Abortar missao !\n";
      exit;
    }
    fclose($ch);

    echo "Continuando...\n";
  }

  private function process() {
    $reader = new PHPExcel_Reader_Excel5();
    foreach ($this->files as $key => $file) {
      echo "Processando planinha " . (int)($key +1) . ": " . $file . "\n";
      $filepath = $this->filesDir . '/' . $file;
      if (file_exists($filepath)) {
        $excel = $reader->load($filepath)->getActiveSheet();
        for ($i=1; $i <= $excel->getHighestRow(); $i++) {
          $row = $excel->getRowIterator($i)->current();
          $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          $row = array();
          foreach ($cellIterator as $cell) {
            $row[] = $cell->getValue();
          }
          if ($this->_insertSemas($row)) {
            print "Salvo na base de dados: linha " . $i . "\n";
          } else {
            print "Ocorreu um erro ao tentar salvar na base de dados a linha " . $i . "\n";
          }
        }

      } else {
        print "Arquivo " . $file . "nao existe. Contunando para o proximo arquivo...\n";
        continue;
      }
    }
  }

  private function _insertSemas($values) {
    $sql = "INSERT INTO carbot.semas (numeroCar, area, tipo, processo, razaoSocial, municipio, tipoSobreposicao, quantidadeSobreposicao, "
      . "area1, area2, area3, area4, area5, area6, area7, area8, somaSobreposicao  )"
      . "VALUES ('" . implode("','", $values) . "')";
    return $this->db->query($sql);
  }

}

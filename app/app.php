<?php

class App {

  private $urlPara;
  private $urlNac;
  private $filesDir;
  private $files = array();
  private $keyPrefix = 'PA-';
  private $db;

  // Inicia variaveis
  public function __construct() {
    require_once 'config.php';
    require_once __DIR__ . '/lib/phpexcel/Classes/PHPExcel.php';
    $this->urlPara = PARA_TARGET_URL;
    $this->urlNac = NAC_TARGET_URL;
    $this->filesDir = FILES_DIR;
  }

  // Executa
  public function run() {
    $this->checkDatabase();
    $this->checkFiles();
    $this->process();
    print ("__FIM__\n");
  }

  /*
   * Verifica conexao com a base e cria schema da aplicacao
   */
  private function checkDatabase() {
    print "Verificando base de dados...\n";
    try {
      $this->db = new PDO("mysql:host=".DB_HOST, DB_USER, DB_PASS, array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
      ));
      $sql = file_get_contents(__DIR__.'/db.sql');
      $exec = $this->db->query($sql);
      if ($exec) {
        print "Base de dados configurada com sucesso !\n";
      } else {
        print "Nao foi possivel criar base de dados. Verifique os parametros de conexao e tente novamente.\n";
        print "Mensagem de erro: " . implode(',', $this->db->errorInfo()) . "\n";
        exit;
      }
    } catch (Exception $ex) {
      print "Erro ao tentar conectar a base de dados. Verifique os parametros de conexao e tente novamente.\n";
      print "Mensagem de erro: " . $ex->getMessage() . "\n";
      exit;
    }
  }

  /**
   * Verifica sistema de arquivos e pede para o usuario continua, dado o numero de arquivos
   * a serem processados
   */
  private function checkFiles() {
    echo "Verificando arquivos disponiveis...\n";
    $dh = opendir($this->filesDir);

    while (false !== ($filename = readdir($dh))) {
      if (substr($filename, -4) == '.xls' || substr($filename, -5) == '.xlsx') {
        $this->files[] = $filename;
      }
    }
    closedir($dh);

    $count = count($this->files);
    if ($count == 0) {
      exit("Não há nenhuma planilha na pasta dos arquivos. Verifique a pasta e tente novamente.\n");
    }

    echo "Existem {$count} planilhas a serem processadas na pasta de arquivos. Continuar o procedimento ?\n";
    echo "Digite qualquer tecla pra continuar ou 'n' para abortar: ";
    $ch = fopen("php://stdin", "r");
    $ansr = fgets($ch);
    if (trim($ansr) == 'n') {
      echo "Abortar missao !\n";
      exit;
    }
    fclose($ch);

    echo "Continuando...\n";
  }

  /**
   * Processa tudo: busca os indices das planilhas, pega no site os dados e salvana base
   */
  private function process() {
    $fileErrors = 0;
    $itemErrors = 0;
    $dbErrors = 0;
    $emptyObjs = 0;
    // Excel reader
    $reader = new PHPExcel_Reader_Excel5();
    foreach ($this->files as $key => $file) {
      echo "Processando planinha " . (int)($key +1) . ": " . $file . "\n";
      $filepath = $this->filesDir . '/' . $file;
      if (file_exists($filepath)) {
        try {
          $excel = $reader->load($filepath)->getActiveSheet();
          for ($i=1; $i <= $excel->getHighestRow(); $i++) {
            $val = $excel->getCellByColumnAndRow(0, $i)->getValue();
            if (stripos($val, ';') !== FALSE) {
              $split = split(';', $val);
              $val = $split[0];
            }
            if (!empty($val) && stripos($val, $this->keyPrefix) === 0) {
              // Pará
              $json = json_decode($this->get($this->urlPara . $val));
              if ($json && json_last_error() === JSON_ERROR_NONE) {
                $save = $this->insertDemonstrativoObj($json);
                if ($save) {
                  //Nacional
                  $nac = json_decode($this->get(str_replace('{ID}', $val, $this->urlNac)));
                  if ($nac && json_last_error() === JSON_ERROR_NONE) {
                    $save2 = $this->insertRazaoSocialObj($nac);
                    if ($save2) {
                      print "Salvo na base de dados: " . $val . "\n";
                    } elseif ($save === '') {
                      $emptyObjs++;
                    print "RazaoSocial vazia: " . $val . ".Demonstrativo salvo.\n";
                  } else {
                    $dbErrors++;
                    print "Ocorreu um erro ao salvar a razao social na base de dados: " . $val . ".Demonstrativo salvo.\n";
                    print "Mensagem de erro: " . implode(',', $this->db->errorInfo()) . "\n";
                  }
                  }
                } elseif ($save === ''){
                  $emptyObjs++;
                  print "Demonstrativo vazio: " . $val . "\n";
                } else {
                  $dbErrors++;
                  print "Ocorreu um erro ao salvar o demonstrativo na base de dados: " . $val . "\n";
                  print "Mensagem de erro: " . implode(',', $this->db->errorInfo()) . "\n";
                }
              } else {
                $itemErrors++;
                print "Ocorreu um erro ao obter as informações do item " . $val . "\n";
              }
            }
          }
        } catch (Exception $ex) {
          $fileErrors++;
          print "Houve um problema ao ler o arquivo " . $file . ":\n" ;
          print $ex->getMessage();
          print "\nContinuando para o proximo arquivo...\n";
          continue;
        }

      } else {
        $fileErrors++;
        print "Arquivo " . $file . "nao existe. Contunando para o proximo arquivo...\n";
        continue;
      }
    }
    if (!empty($fileErrors || !empty($itemErrors) || !empty($dbErrors))) {
      print "__ERROS_ENCONTRADOS__\n";
      print "Arquivos: " . $fileErrors. "\n";
      print "Items: " . $itemErrors. "\n";
      print "Base de Dados: " . $dbErrors. "\n";
    }

    if (!empty($emptyObjs)) {
      print "__OBSERVACOES__\n";
      print "Objetos vazios: " . $emptyObjs. "\n";
    }
  }

  /**
   * GET em uma url
   * @param string $url
   * @return string
   */
  private function get($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $return = curl_exec($ch);
    curl_close($ch);
    return $return;
  }

  /**
   * Insere objeto demonstrativo/imovel na base de dados
   * @param type $json O objeto JSON bruto vindo do site
   * @return mixed Retorna um objeto em caso de sucesso, FALSE em caso de falha e string vazia para objeto vazio
   */
  private function insertDemonstrativoObj($json) {
    if (is_object($json->dados->imovel)) {
      $checkSql = "SELECT id FROM carbot.imovel WHERE codigoImovel = '{$json->dados->imovel->codigoImovel}'";
      $check = $this->db->query($checkSql)->fetchAll();
      if (empty($check)) {
        return $this->_insertImovel($json->dados->imovel);
      } else {
        $idx = reset($check);
        return $this->_updateImovel($idx['id'], $json->dados->imovel);
      }

    } elseif (is_object($json->dados->demonstrativo)) {
      $checkSql = "SELECT id FROM carbot.demonstrativo WHERE codigoImovel = '{$json->dados->demonstrativo->codigoImovel}'";
      $check = $this->db->query($checkSql)->fetchAll();
      if (empty($check)) {
        return $this->_insertDemonstrativo($json->dados->demonstrativo);
      } else {
        $idx = reset($check);
        return $this->_updateDemonstrativo($idx['id'], $json->dados->demonstrativo);
      }
    }

    return '';
  }

 /**
   * Insere objeto razaoSocial na base de dados
   * @param type $json O objeto JSON bruto vindo do site
   * @return mixed Retorna um objeto em caso de sucesso, FALSE em caso de falha e string vazia para objeto vazio
   */
  private function insertRazaoSocialObj($json) {
   if (is_object($json->dados)) {
     $checkSql = "SELECT id FROM carbot.razaoSocial WHERE codigoImovel = '{$json->dados->codigoImovel}'";
     $check = $this->db->query($checkSql)->fetchAll();
     if (empty($check)) {
       return $this->_insertRazaoSocial($json->dados);
     } else {
       $idx = reset($check);
       return $this->_updateRazaoSocial($idx['id'], $json->dados);
     }
   }

   return '';
  }

  /**
   * Insere RazaoSocial
   * @param type $obj Objeto da RazaoSocial
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _insertRazaoSocial($obj) {
    $keys = array();
    $values = array();
    $clone = clone $obj;
    $clone->municipio = $obj->municipio->nome;
    $clone->estado = $obj->municipio->estado->id;
    unset($clone->id);
    $clone->idExterno = $obj->id;
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $sql = "INSERT INTO carbot.razaoSocial (" . implode(',', $keys) .  ") " .
      "VALUES ('" . implode("','", $values) . "')";
    return $this->db->query($sql);
  }

  /**
   * Atualiza RazaoSocial
   * @param type $obj Objeto da RazaoSocial
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _updateRazaoSocial($id, $obj) {
    $keys = array();
    $values = array();
    $clone = clone $obj;
    $clone->municipio = $obj->municipio->nome;
    $clone->estado = $obj->municipio->estado->id;
    unset($clone->id);
    $clone->idExterno = $obj->id;
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $up = $this->_parseUpdateQueryKeyVals($keys, $values);

    $sql = "UPDATE carbot.razaoSocial SET ".$up." WHERE id = ".$id;
    return $this->db->query($sql);
  }

  /**
   * Insere Imovel
   * @param type $obj Objeto do imovel
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _insertImovel($obj) {
    $keys = array();
    $values = array();
    $clone = clone $obj;
    $clone->municipio = $obj->municipio->nome;
    $clone->estado = $obj->municipio->estado->id;
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $sql = "INSERT INTO carbot.imovel (" . implode(',', $keys) .  ") " .
      "VALUES ('" . implode("','", $values) . "')";
    return $this->db->query($sql);
  }

  /**
   * Atualiza Imovel
   * @param type $obj Objeto do imovel
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _updateImovel($id, $obj) {
    $keys = array();
    $values = array();
    $clone = clone $obj;
    $clone->municipio = $obj->municipio->nome;
    $clone->estado = $obj->municipio->estado->id;
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $up = $this->_parseUpdateQueryKeyVals($keys, $values);

    $sql = "UPDATE carbot.imovel SET ".$up." WHERE id = ".$id;
    return $this->db->query($sql);
  }

  /**
   * Insere Demonstrativo
   * @param type $obj Objeto do demonstrativo
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _insertDemonstrativo($obj) {
    $keys = array();
    $values = array();
    $clone = clone $obj;
    unset($clone->restricaoImovel);
    unset($clone->sobreposicaoImovel);
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $sql = "INSERT INTO carbot.demonstrativo (" . implode(',', $keys) .  ") " .
      "VALUES ('" . implode("','", $values) . "')";

    if(!$this->db->query($sql)) {
      return FALSE;
    }
    $idDemonst = $this->db->lastInsertId();

    if (!empty($obj->restricaoImovel)) {
      foreach ($obj->restricaoImovel as $restric) {
        if (!$this->_insertRelatedToDemonstrativo('restricaoImovel', $restric, $idDemonst)) {
          return FALSE;
        } else {
          continue;
        }
      }
    }

    if (!empty($obj->sobreposicaoImovel)) {
      foreach ($obj->sobreposicaoImovel as $sobrep) {
        if (!$this->_insertRelatedToDemonstrativo('sobreposicaoImovel', $sobrep, $idDemonst)) {
          return FALSE;
        } else {
          continue;
        }
      }
    }

    return TRUE;
  }

  /**
   * Atualiza Demonstrativo
   * @param type $obj Objeto do demonstrativo
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _updateDemonstrativo($id, $obj) {
    $clone = clone $obj;
    unset($clone->restricaoImovel);
    unset($clone->sobreposicaoImovel);
    $keys = array();
    $values = array();
    $this->_parseQueryKeyVals($clone, $keys, $values);

    $up = '';
    foreach($values as $key => $val) {
      $up .= $keys[$key] . "='" . $val . "'";
      if ($key != count($keys) - 1) {
        $up .= ",";
      }
    }

    $sql = "UPDATE carbot.demonstrativo SET ".$up." WHERE id = ".$id;
    return $this->db->query($sql);
  }

  /**
   * Grava objeto relativo ao demonstrativo
   * @param type $table A tabela em questao (restricaoImovel ou sobreposicaoImovel)
   * @param type $obj O objeto
   * @param type $idDemonst O id do demonstrativo a ser relacionado
   * @return type Retorna um objeto em caso de sucesso, FALSE em caso de falha
   */
  private function _insertRelatedToDemonstrativo($table, $obj, $idDemonst) {
    $keys = array();
    $values = array();
    $obj->idDemonstrativo = $idDemonst;
    $this->_parseQueryKeyVals($obj, $keys, $values);
    $sql = "INSERT INTO carbot.{$table} (" . implode(',', $keys) .  ") " .
      "VALUES ('" . implode("','", $values) . "')";
    return $this->db->query($sql);
  }

  /**
   * Funcao auxiliar para parsear variaveis de um objeto para uma query string
   * @param type $obj O objeto a ser parseado
   * @param type $keys Ponteiro para nome dos atributos
   * @param type $values Ponteiro para valores dos atributos
   */
  private function _parseQueryKeyVals($obj, &$keys, &$values) {
    foreach ($obj as $key => $val) {
      if (!is_array($val) && !is_object($val)) {
        $keys[] = $key;
        $values[] = str_replace(array('"', "'"), '', $val);
      }
    }
  }

  /**
   * Função auxiliar para parsear chaves e valores de uma query de update
   * @param type $keys
   * @param type $values
   * @return string A parte do 'SET' de uma query de update
   */
  private function _parseUpdateQueryKeyVals($keys, $values) {
    $up = '';
    foreach($values as $key => $val) {
      $up .= $keys[$key] . "='" . $val . "'";
      if ($key != count($keys) - 1) {
        $up .= ",";
      }
    }
    return $up;
  }

}

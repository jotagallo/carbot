CREATE SCHEMA IF NOT EXISTS `carbot` DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`demonstrativo` (
`id` INT NOT NULL AUTO_INCREMENT,
  `areaAppRecompor` FLOAT NULL,
  `areaImovel` FLOAT NULL,
  `demonstrativocol` FLOAT NULL,
  `areaReservaLegalMinimaLei` FLOAT NULL,
  `areaReservaLegalRecompor` FLOAT NULL,
  `areaUsoAlternativoSolo` FLOAT NULL,
  `areaUsoRestritoRecompor` FLOAT NULL,
  `artCadastrante` MEDIUMTEXT NULL,
  `codigoImovel` MEDIUMTEXT NULL,
  `codigoProtocolo` MEDIUMTEXT NULL,
  `dataAtualizacao` MEDIUMTEXT NULL,
  `dataCadastro` MEDIUMTEXT NULL,
  `latitudeImovel` MEDIUMTEXT NULL,
  `longitudeImovel` MEDIUMTEXT NULL,
  `moduloFiscal` FLOAT NULL,
  `nomeCadastrante` MEDIUMTEXT NULL,
  `nomeMunicipio` MEDIUMTEXT NULL,
  `registroCadastrante` MEDIUMTEXT NULL,
  `siglaEstado` MEDIUMTEXT NULL,
  `statusImovel` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`imovel` (
`id` INT NOT NULL AUTO_INCREMENT,
  `areaImovel` FLOAT NULL,
  `codigoImovel` MEDIUMTEXT NULL,
  `dataCriacao` MEDIUMTEXT NULL,
  `idImovel` INT(11) NULL,
  `modulosFiscais` FLOAT NULL,
  `municipio` MEDIUMTEXT NULL,
  `estado` MEDIUMTEXT NULL,
  `nomeCadastrante` MEDIUMTEXT NULL,
  `nomeImovel` MEDIUMTEXT NULL,
  `protocolo` MEDIUMTEXT NULL,
  `statusImovel` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`restricaoImovel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idDemonstrativo` INT(11) NULL,
  `areaConflito` FLOAT NULL,
  `dataProcessamento` MEDIUMTEXT NULL,
  `descricao` MEDIUMTEXT NULL,
  `origem` MEDIUMTEXT NULL,
  `percentualConflito` FLOAT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`sobreposicaoImovel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idDemonstrativo` INT(11) NULL,
  `areaSobreposicao` FLOAT NULL,
  `codigoImovel` MEDIUMTEXT NULL,
  `percentualSobreposicao` FLOAT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`razaoSocial` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idExterno` INT(11) NULL,
  `codigoImovel` MEDIUMTEXT NULL,
  `protocolo` MEDIUMTEXT NULL,
  `nomeCadastrante` MEDIUMTEXT NULL,
  `nomeImovel` MEDIUMTEXT NULL,
  `municipio` MEDIUMTEXT NULL,
  `estado` MEDIUMTEXT NULL,
  `areaImovel` FLOAT NULL,
  `modulosFiscais` FLOAT NULL,
  `dataCriacao` MEDIUMTEXT NULL,
  `isAtivo` INT(11) NULL,
  `statusImovel` MEDIUMTEXT NULL,
  `condicaoAnalise` MEDIUMTEXT NULL,
  `idImovelPosterior` MEDIUMTEXT NULL,
  `passivelAdesaoPra` MEDIUMTEXT NULL,
  `possuiRestricoes` MEDIUMTEXT NULL NULL,
  `dataConsulta` MEDIUMTEXT NULL,
  `horaConsulta` MEDIUMTEXT NULL,
  `bbox` MEDIUMTEXT NULL,
  `substituiImovelPorDuplicidade` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `carbot`.`semas`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `numeroCar` MEDIUMTEXT NULL,
  `area` MEDIUMTEXT NULL, 
  `tipo` MEDIUMTEXT NULL,
  `processo` MEDIUMTEXT NULL,
  `razaoSocial` MEDIUMTEXT NULL,
  `municipio` MEDIUMTEXT NULL,
  `tipoSobreposicao` MEDIUMTEXT NULL,
  `quantidadeSobreposicao` MEDIUMTEXT NULL,
  `area1` MEDIUMTEXT NULL,
  `area2` MEDIUMTEXT NULL,
  `area3` MEDIUMTEXT NULL,
  `area4` MEDIUMTEXT NULL,
  `area5` MEDIUMTEXT NULL,
  `area6` MEDIUMTEXT NULL,
  `area7` MEDIUMTEXT NULL,
  `area8` MEDIUMTEXT NULL,
  `somaSobreposicao` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
# CARBOT - Robo automatizado para salvar dados do CAR. #

### Autor: João Otávio Gallo <joao.otavio.gallo@gmail.com>
Desenvolvido especialmente para a Agência Pública ###

Dependências:
- PHP <= 5.5 (curl, gd, pdo, pdo_mysql)
- MySQL (5.x)
- Ambientes Unix (OSX, Linux, BSD)

Instruções:

- Colocar planilhas com os dados de imóveis na pasta 'files'
- Setar variáveis de conexão ao banco no arquivo app/config.php (váriáveis DB_*, requer acesso root)
- No terminal, navegar até a pasta e rodar o comando 'sh run.sh'
- Alternativamente, rodar os seguintes comandos na pasta: 'chmod +x run.sh' e './run.sh'

Dúvidas, sugestões ou correções favor entrar em contato.
